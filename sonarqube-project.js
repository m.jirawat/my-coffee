const scanner = require("sonarqube-scanner");
scanner(
  {
    serverUrl: "http://localhost:9000",
    token: "sqp_f87e270abd7d269441f6ffde1d2c97bfdf4bb604",
    options: {
      "sonar.projectName": "my-coffee",
      "sonar.sources": "src",
    },
  },
  () => process.exit()
);
